# Parser Web Crawler
This project aims to create a web crawler for Parser.

It implements a command line interface that exposes the web crawler interface to the public.
See instructions below about how to use it.

This crawler follows the rules below:
* Given a starting URL, the crawler should visit each URL it finds on the same domain;
* It should print each URL visited, and a list of links found on that page;
* The crawler should be limited to one subdomain (i.e. if the starting url is `https://parserdigital.com/`, 
do not follow external links, for example to facebook.com or community.parserdigital.com);

The crawler is composed by the following elements:

<img src="./static/crawler.png" width="400" height="500">

* `Crawler` - This Crawler orchestrates all the crawling logic; It handles the logic where a link should
be added to the list of printed links or even whether it should crawl the link or not. It also handles a pool
of workers (a.k.a. crawlers) to extract links concurrently;
* `Extractor` - It extracts urls from a given link;
* `ThrottleExtractor` - It throttles the HTTP requests so it doesn't trigger a 429 HTTP code from websites. This component is optional and will be added only on (argument) request. 
* `Printer` - It prints all the visited links and a list with the respective links inside them where
`->` represents the parent link and `#` the links inside the parent one.
```
i.e. https://google.com page has 4 links inside the html page. Each of them has a different domain/subdomain
so it will only crawl over https://google.com/about that has the same domain.
Then, https://google.com/about has 2 links inside its html page which won't be crawled recursively because
they either have a different domain/subdomain or have been crawled already to avoid infinite crawling.

 -> https://google.com
   # https://mail.gmail.com
   -> https://google.com/about
     # https://google.com
     # https://youtube.com
   # https://drive.gmail.com
   # https://wikipedia.com 
```

### CLI arguments available
To use the CLI, you should either run the crawler command or the binary with the optional arguments:
```
# Run the command on the root of the project
go run cmd/crawler/main.go <optional arguments>

# Run the binary after building it
./bin/parser-web-crawler <optional arguments>
```
The available/optional arguments are:
```
* -link - Set the starting link for the crawler. Default value is `https://parserdigital.com/`;
* -cliTimeout - Set the cli timeout in seconds. Default value is `30`s;
* -httpTimeout - Set the http client timeout in seconds. Default value is `5`s;
* -crawlers - Set thee number of concurrent workers. Default value is `30`;
* -v - Run the command on verbose mode (i.e. print errors). Default value is `false`;
* -throttle - Set thenumber of http requests/second. Setting it to 0 means no throttling. Default value is `0`.
```

### Bottlenecks found during the development road
As soon as the simple web crawler was working (single threaded), it was noticed that the majority of the time
spent was waiting for the HTTP responses. It was mainly due to the time that the request needs to travel
over the internet.

#### Concurrency
To fix this HTTP call waiting time, concurrency was added to the crawler. Whether it's just 2 workers
or 100, now the crawler has way less cooldown time while making the http requests because it can do a few of them
concurrently.

However, it raised another problem. Some websites are more intelligent as others and, when the crawler raids
them with a lot of HTTP requests coming from the same IP, these websites block all the incoming requests and
start sending back responses with HTTP code 429 (Too Many Requests).

#### Throttling
One way to work it around is to add a throttler to limit the number of HTTP requests per second.
After adding it, I've found that I needed to try out different parameters (like changing the number of workers)
until the crawler was able to crawl these smart websites. Therefore, I've added a feature that throttles the HTTP
calls. Still, if the crawler receives a 429, it doubles the throttling period again and again until it finds the
sweet spot for the throttling period (429 HTTP codes are no longer received).

### Wasted resources
We have to take into account that, since we are using a pool of workers, if we set some throttling, probably
a few workers (go routines) will be created but won't do anything since the throttler ends up limiting them.
There's a high chance that memory is being used in a useless way (even though it might not be that much since
go routines start up with 2KB).
This issue can be handled later and was added to the future work session in the end of the `README.md`.

## How to run it

### Prerequisites
* Install:
    * Go 1.21.* (other versions work as well) - `brew install go`
    * Make - `brew install make`
    * Golangci-lint (optional - for development only) - `brew install golangci-lint`

(Note that this is for MacOS. Do the respective on Linux or Windows)

### Start the Crawler
```
# Run locally
make start
# or build binaries and run them
make build
./bin/parser-web-crawler

# If you need to download go dependencies.
make mod
```

There are other useful commands for development purposes:
```
# Build the project
make build

# Run tests
make test

# Run tests
make test-all

# Run tests with coverage
make test-cover
```

## Development process
During the development process, there were a few guidelines that were followed like testing the code with
unit tests and use a few linters through golangci-lint.

In order to avoid forgetting about the guidelines, there is a [command](./cmd/dev/README.md)
added to this project that sets up the development environment. Read the command documentation to
understand how to use it.

## Future work
As a future work, there's still the chance to implement a feature that reduces the number of workers from
the pool when the throttler increases its rate limit. For now it didn't make sense to implement it since
this is supposed to be a simple web crawler.