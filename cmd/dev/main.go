package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	fmt.Println("starting up dev env...")

	srcDir := "./scripts/git-hooks"
	dstDir := "./.git/hooks"

	dirEntries, err := os.ReadDir(srcDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, entry := range dirEntries {
		if !entry.IsDir() {
			if err = copyFile(
				fmt.Sprintf("%s/%s", srcDir, entry.Name()),
				fmt.Sprintf("%s/%s", dstDir, entry.Name()),
			); err != nil {
				log.Fatal(err)
			}
		}
	}

	fmt.Println("start up finished with success!")
}

// Be mindful about the passed src and dst arguments. Don't provide paths that
// should not be accessed by non-privileged users or this function might have a
// security breech by copying malicious files to protected paths.
func copyFile(src, dst string) error {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	//nolint:gosec // Disabling gosec for this line since it's only copying project
	// scripts to git hooks folder (by being called by dev cmd).
	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer fileClose(source)

	//nolint:gosec // Disabling gosec for this line since it's only copying project
	// scripts to git hooks folder (by being called by dev cmd).
	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer fileClose(destination)
	_, err = io.Copy(destination, source)
	return err
}

func fileClose(file *os.File) {
	err := file.Close()
	if err != nil {
		log.Fatal(err)
	}
}
