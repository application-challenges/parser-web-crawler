# Start up Dev Env command
This simple script aims to start up the dev env.

For now, it only copies git hook scripts from `$PROJECT_ROOT/scripts/` to `.git/hooks/` so 
whoever commits to the repository doesn't commit faulty/unindented/unsecured code.

This script has to be run from the root directory of the project.