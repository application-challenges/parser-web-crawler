package main

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"parser-web-crawler/internal/log"
	"parser-web-crawler/internal/url"
	"parser-web-crawler/internal/web"
	"time"
)

var (
	// cliTimeout sets the timeout for the whole cli in seconds.
	// Big websites might have a lot of links so the crawler might take a while so
	// use this timeout as an exit if the crawler takes too long.
	//
	// For smaller concurrency requests, this timeout might not be enough. Increase it
	// as it's needed.
	cliTimeout  = new(int)
	httpTimeout = new(int)
	crawlers    = new(int)
	verbose     = new(bool)
	link        = new(string)
	throttle    = new(int)
)

func init() {
	flag.StringVar(link, "link", "https://parserdigital.com/", "set link to crawl")
	flag.IntVar(cliTimeout, "cliTimeout", 30, "cli timeout in seconds")
	flag.IntVar(httpTimeout, "httpTimeout", 5, "http timeout in seconds")
	flag.IntVar(crawlers, "crawlers", 30, "number of concurrent crawlers")
	flag.BoolVar(verbose, "v", false, "run command on verbose mode (i.e. print errors")
	flag.IntVar(throttle, "throttle", 0, "number of http requests/second. Setting it to 0 means no throttling")
	flag.Parse()
}

func main() {
	var (
		httpClient = &http.Client{
			Timeout: time.Duration(*httpTimeout) * time.Second,
		}
		urlExtractor url.Extractor = url.NewHTMLExtractor(httpClient)
		urlPrinter                 = url.NewPrinter(os.Stdout)
	)

	if *throttle != 0 {
		urlExtractor = url.NewThrottlingExtractor(urlExtractor, *throttle, *crawlers)
	}

	webCrawler := web.NewCrawler(&log.Logger{Verbose: *verbose}, urlExtractor, urlPrinter, *crawlers)

	ctx := context.Background()
	if *cliTimeout != 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(context.Background(), time.Duration(*cliTimeout)*time.Second)
		defer cancel()
	}

	fmt.Printf("Crawling over %s...\n", *link)
	if err := webCrawler.Crawl(ctx, *link); err != nil {
		slog.Error(err.Error())
		return
	}
}
