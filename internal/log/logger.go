package log

import (
	"log/slog"
)

// Logger logs errors.
type Logger struct {
	Verbose bool
}

// Err logs an error. Based on Verbose config or not, it logs it at LevelError
// or DebugError.
func (l *Logger) Err(err error, url string) {
	if l.Verbose {
		slog.Error(err.Error(), "url", url)
		return
	}

	slog.Debug(err.Error(), "url", url)
}
