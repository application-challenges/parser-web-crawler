package web

import (
	"context"
	neturl "net/url"
	"parser-web-crawler/internal/url"
	"strings"
	"sync"

	"github.com/juju/errors"
)

// Logger logs errors.
type Logger interface {
	Err(err error, url string)
}

// URLPrinter prints urls.
type URLPrinter interface {
	Print(urls *url.Node) error
}

// Crawler crawls over HTML pages and prints all hyperlinks that it contains.
type Crawler struct {
	logger Logger

	urlExtractor url.Extractor
	urlPrinter   URLPrinter

	seenURLs map[string]struct{}

	// crawlers are an a.k.a. for concurrent workers
	crawlers int
	// crawlersCounter counts the number of active crawlers that are extracting link.
	crawlersCounter int
	crawlChan       chan crawlInput
	resultChan      chan resultOutput
}

// NewCrawler returns a new instance of Crawler.
func NewCrawler(
	logger Logger,
	urlExtractor url.Extractor,
	urlPrinter URLPrinter,
	crawlersNum int,
) *Crawler {
	wg := sync.WaitGroup{}
	wg.Add(1)
	c := &Crawler{
		logger: logger,

		urlExtractor: urlExtractor,
		urlPrinter:   urlPrinter,

		seenURLs: map[string]struct{}{},

		crawlers:   crawlersNum,
		crawlChan:  make(chan crawlInput),
		resultChan: make(chan resultOutput),
	}

	// Start up crawlers
	for i := 0; i < c.crawlers; i++ {
		go c.crawler()
	}

	return c
}

// Crawl crawls over an HTML page, given a link, and prints all hyperlinks that it contains.
func (c *Crawler) Crawl(ctx context.Context, parentLink string) error {
	parentLink = strings.ReplaceAll(parentLink, " ", "")
	c.startCrawling(ctx, parentLink)

	parentURL, err := neturl.Parse(parentLink)
	if err != nil {
		return errors.Trace(err)
	}

	urls := map[string][]*neturl.URL{}
	for ; c.crawlersCounter > 0; c.crawlersCounter-- {
		output := <-c.resultChan
		if output.err != nil {
			c.logger.Err(output.err, output.parentLink)
			continue
		}

		var validURLs []*neturl.URL
		for _, u := range output.extractedURLs {
			// Other domains are not even added to the list of URL children to show.
			if u.Scheme != "https" && u.Scheme != "http" {
				continue
			}

			validURLs = append(validURLs, u)

			if _, ok := c.seenURLs[u.String()]; ok {
				continue
			}

			c.seenURLs[u.String()] = struct{}{}

			if u.Host != parentURL.Host {
				continue
			}

			c.crawlersCounter++
			c.crawlChan <- crawlInput{
				ctx:  ctx,
				link: u.String(),
			}
		}

		urls[output.parentLink] = validURLs
	}

	parentNode := url.NewNode(parentLink, urls)

	return c.urlPrinter.Print(parentNode)
}

func (c *Crawler) startCrawling(ctx context.Context, parentLink string) {
	c.crawlersCounter++
	c.crawlChan <- crawlInput{
		ctx:  ctx,
		link: parentLink,
	}
	c.seenURLs[parentLink] = struct{}{}
}

func (c *Crawler) crawler() {
	for {
		input := <-c.crawlChan

		urls, err := c.urlExtractor.Extract(input.ctx, input.link)

		go func() {
			c.resultChan <- resultOutput{
				parentLink:    input.link,
				extractedURLs: urls,
				err:           err,
			}
		}()
	}
}

type crawlInput struct {
	ctx  context.Context
	link string
}

type resultOutput struct {
	parentLink    string
	extractedURLs []*neturl.URL
	err           error
}
