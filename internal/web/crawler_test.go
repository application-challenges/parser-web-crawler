package web

import (
	"context"
	neturl "net/url"
	"parser-web-crawler/internal/url"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCrawler_crawler(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		urlExtractor := &mockURLExtractor{}
		crawler := NewCrawler(nil, urlExtractor, nil, 0)

		go crawler.crawler()
		go crawler.crawler()

		for i := 0; i < 20; i++ {
			go func() {
				crawler.crawlChan <- crawlInput{
					ctx:  context.Background(),
					link: "https://many.com/empty",
				}
			}()
		}

		for i := 0; i < 20; i++ {
			result := <-crawler.resultChan
			assert.Nil(t, result.err)
			assert.Equal(t, "https://many.com/empty", result.parentLink)
			assert.Equal(t, []*neturl.URL(nil), result.extractedURLs)
		}

		assert.Equal(t, urlExtractor.count, 20)
	})
}

func TestCrawler_startCrawling(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		crawler := NewCrawler(nil, &mockURLExtractor{}, nil, 1)

		crawler.startCrawling(context.Background(), "https://many.com/hello")

		result := <-crawler.resultChan
		assert.Nil(t, result.err)
		assert.Equal(t, "https://many.com/hello", result.parentLink)
		assert.Equal(t, []*neturl.URL{{
			Scheme: "https",
			Host:   "many.com",
		}}, result.extractedURLs)
	})

	t.Run("error", func(t *testing.T) {
		crawler := NewCrawler(nil, &mockURLExtractor{}, nil, 2)

		crawler.startCrawling(context.Background(), "error.com")

		response := <-crawler.resultChan
		assert.ErrorIs(t, response.err, assert.AnError)
	})
}

func TestCrawler_Crawl(t *testing.T) {
	urlExtractor := &mockURLExtractor{}
	urlPrinter := &mockURLPrinter{}
	crawler := NewCrawler(nil, urlExtractor, urlPrinter, 1)

	t.Run("avoid_seen_link_extraction", func(t *testing.T) {
		err := crawler.Crawl(context.Background(), "https://many.com")

		assert.Nil(t, err)
		assert.Equal(t, 3, urlExtractor.count)
		assert.Equal(t, &url.Node{
			Link: "https://many.com",
			Children: []*url.Node{
				{
					Link: "https://many.com/empty",
				},
				{
					Link: "https://many.com/hello",
				},
			},
		}, urlPrinter.node)
	})

	t.Run("only_domain", func(t *testing.T) {
		urlExtractor.resetCounter()

		err := crawler.Crawl(context.Background(), "https://many.com/out")

		assert.Nil(t, err)
		assert.Equal(t, 1, urlExtractor.count)
		assert.Equal(t, &url.Node{
			Link: "https://many.com/out",
			Children: []*url.Node{
				{
					Link: "https://out.com",
				},
			},
		}, urlPrinter.node)
	})

	t.Run("avoid_subdomain_extraction", func(t *testing.T) {
		urlExtractor.resetCounter()

		err := crawler.Crawl(context.Background(), "https://many.com/subdomain")

		assert.Nil(t, err)
		assert.Equal(t, 1, urlExtractor.count)
		assert.Equal(t, &url.Node{
			Link: "https://many.com/subdomain",
			Children: []*url.Node{
				{
					Link: "https://subdomain.many.com",
				},
			},
		}, urlPrinter.node)
	})

	t.Run("avoid_non_http(s)_schemes", func(t *testing.T) {
		urlExtractor.resetCounter()

		err := crawler.Crawl(context.Background(), "https://many.com/ftp")

		assert.Nil(t, err)
		assert.Equal(t, 1, urlExtractor.count)
		assert.Equal(t, &url.Node{
			Link: "https://many.com/ftp",
		}, urlPrinter.node)
	})
}

type mockURLExtractor struct {
	count int
	// sync.Muted added for testing purposes only.
	mutex sync.Mutex
}

func (e *mockURLExtractor) resetCounter() {
	e.mutex.Lock()
	e.count = 0
	e.mutex.Unlock()
}

func (e *mockURLExtractor) Extract(_ context.Context, link string) ([]*neturl.URL, error) {
	e.mutex.Lock()
	e.count++
	e.mutex.Unlock()

	switch {
	case link == "https://many.com/ftp":
		return []*neturl.URL{{
			Scheme: "ftp",
			Host:   "many.com",
		}}, nil
	case link == "https://many.com/subdomain":
		return []*neturl.URL{{
			Scheme: "https",
			Host:   "subdomain.many.com",
		}}, nil
	case link == "https://many.com/out":
		return []*neturl.URL{{
			Scheme: "https",
			Host:   "out.com",
		}}, nil
	case link == "https://many.com/empty":
		return nil, nil
	case link == "https://many.com/hello":
		return []*neturl.URL{{
			Scheme: "https",
			Host:   "many.com",
		}}, nil
	case link == "https://many.com":
		return []*neturl.URL{
			{
				Scheme: "https",
				Host:   "many.com",
				Path:   "/empty",
			},
			{
				Scheme: "https",
				Host:   "many.com",
				Path:   "/hello",
			},
		}, nil
	}

	return nil, assert.AnError
}

type mockURLPrinter struct {
	node *url.Node
}

func (p *mockURLPrinter) Print(urls *url.Node) error {
	p.node = urls
	return nil
}
