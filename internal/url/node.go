package url

import (
	neturl "net/url"
)

// Node represents a collection of hierarchical URLs where each URL (node) consists of
// a link and a list of references to its URL children.
type Node struct {
	Link     string
	Children []*Node
}

// NewNode returns a new instance of Node. To build the node tree structure, it
// requires a map of links and the respective children linked URLs and the parent link.
func NewNode(parentLink string, urls map[string][]*neturl.URL) *Node {
	var (
		root  = &Node{Link: parentLink}
		seen  = map[string]*Node{parentLink: root}
		queue = []*Node{root}
	)

	for len(queue) > 0 {
		current := queue[0]
		queue = queue[1:]

		childrenURLs, ok := urls[current.Link]
		if !ok {
			continue
		}

		for _, childURL := range childrenURLs {
			childLink := childURL.String()

			if _, ok = seen[childLink]; ok {
				continue
			}

			child := &Node{Link: childLink}
			current.Children = append(current.Children, child)
			seen[childLink] = child
			queue = append(queue, child)
		}
	}

	return root
}
