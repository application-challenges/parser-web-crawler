package url

import (
	"context"
	neturl "net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestThrottleExtractor_Extract(t *testing.T) {
	requestsPerSecond := 1000 // lots of requests/sec so tests run fast
	initialRate := time.Second / time.Duration(requestsPerSecond)
	throttleExtractor := NewThrottlingExtractor(&mockExtractor{}, requestsPerSecond, 1)

	t.Run("success_no_throttling", func(t *testing.T) {
		assert.Equal(t, initialRate, throttleExtractor.requestsPerSecond)

		urls, err := throttleExtractor.extract(context.Background(), "https://hello.com/index.html#introduction")
		assert.Nil(t, err)
		assert.Equal(t, []*neturl.URL{{
			Scheme:   "https",
			Host:     "hello.com",
			Path:     "index.html",
			Fragment: "#introduction",
		}}, urls)
		assert.Equal(t, initialRate, throttleExtractor.requestsPerSecond)
	})

	t.Run("throttling", func(t *testing.T) {
		throttledExtractor := NewThrottlingExtractor(&mockExtractor{}, requestsPerSecond, 1)

		assert.Equal(t, initialRate, throttledExtractor.requestsPerSecond)

		_, err := throttledExtractor.extract(context.Background(), "https://429.com")
		assert.ErrorIs(t, err, nil)
		assert.Equal(t, initialRate*2, throttledExtractor.requestsPerSecond)
	})

	t.Run("error", func(t *testing.T) {
		assert.Equal(t, initialRate, throttleExtractor.requestsPerSecond)

		_, err := throttleExtractor.extract(context.Background(), "https://error.com")
		assert.ErrorIs(t, err, assert.AnError)
		assert.Equal(t, initialRate, throttleExtractor.requestsPerSecond)
	})
}

type mockExtractor struct{}

func (m *mockExtractor) Extract(ctx context.Context, link string) ([]*neturl.URL, error) {
	if link == "https://error.com" {
		return nil, assert.AnError
	}

	if link == "https://429.com" {
		return nil, ErrTooManyRequests
	}

	return []*neturl.URL{{
		Scheme:   "https",
		Host:     "hello.com",
		Path:     "index.html",
		Fragment: "#introduction",
	}}, nil
}
