package url

import (
	"fmt"
	"io"

	"github.com/juju/errors"
)

// Printer is responsible for printing URLs to a writer.
type Printer struct {
	writer io.Writer
}

// NewPrinter returns a new instance of Printer.
func NewPrinter(writer io.Writer) *Printer {
	return &Printer{
		writer: writer,
	}
}

// Print prints URLs to a writer.
func (p *Printer) Print(urls *Node) error {
	fmt.Printf("\n\nHere are the links found from %s:\n", urls.Link)

	return p.printNode(urls, "")
}

func (p *Printer) printNode(node *Node, prefix string) error {
	if node == nil {
		return nil
	}

	_, err := fmt.Fprintf(p.writer, "%s-> %s\n", prefix, node.Link)
	if err != nil {
		return errors.Trace(err)
	}

	for _, child := range node.Children {
		if child.Children == nil {
			_, err := fmt.Fprintf(p.writer, "%s  # %s\n", prefix, child.Link)
			if err != nil {
				return errors.Trace(err)
			}

			continue
		}

		if err = p.printNode(child, prefix+"  "); err != nil {
			return errors.Trace(err)
		}
	}

	return nil
}
