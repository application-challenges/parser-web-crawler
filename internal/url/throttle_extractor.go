package url

import (
	"context"
	neturl "net/url"
	"time"

	"github.com/juju/errors"
)

// ThrottlingExtractor extracts URLs but adds a throttling layer to the http calls.
type ThrottlingExtractor struct {
	extractor Extractor

	requestsPerSecond time.Duration
	ticker            *time.Ticker

	queue chan queueRequest
}

// NewThrottlingExtractor returns a new instance of ThrottlingExtractor.
func NewThrottlingExtractor(extractor Extractor, requestsPerSecond, queueLimit int) *ThrottlingExtractor {
	var (
		rps    = time.Second / time.Duration(requestsPerSecond)
		ticker = time.NewTicker(rps)
		queue  = make(chan queueRequest, queueLimit)
	)

	throttler := &ThrottlingExtractor{
		extractor:         extractor,
		requestsPerSecond: rps,
		ticker:            ticker,
		queue:             queue,
	}

	go throttler.tick()

	return throttler
}

// Extract extracts URLs but adds a throttling layer to the http calls.
func (e *ThrottlingExtractor) Extract(ctx context.Context, link string) ([]*neturl.URL, error) {
	resultChan := make(chan queueResult)

	e.queue <- queueRequest{
		link:       link,
		ctx:        ctx,
		resultChan: resultChan,
	}

	result := <-resultChan
	return result.urls, result.err
}

func (e *ThrottlingExtractor) extract(ctx context.Context, link string) ([]*neturl.URL, error) {
	urls, err := e.extractor.Extract(ctx, link)
	if errors.Is(err, ErrTooManyRequests) {
		e.requestsPerSecond *= 2
		e.ticker.Reset(e.requestsPerSecond)

		// just like any other HTTP code, we don't care about it but for throttling,
		// so it returns a nil error after being handled.
		return urls, nil
	}

	return urls, errors.Trace(err)
}

func (e *ThrottlingExtractor) tick() {
	for range e.ticker.C {
		go func() {
			request := <-e.queue
			urls, err := e.extract(request.ctx, request.link)
			request.resultChan <- queueResult{
				urls: urls,
				err:  err,
			}
		}()
	}
}

type queueRequest struct {
	link       string
	ctx        context.Context
	resultChan chan queueResult
}

type queueResult struct {
	urls []*neturl.URL
	err  error
}
