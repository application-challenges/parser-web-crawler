package url

import (
	"errors"
)

// ErrTooManyRequests is returned when the http clients receives a status code of 429.
var ErrTooManyRequests = errors.New("url: too many requests")
