package url

import (
	"context"
	"net/http"
	neturl "net/url"
	"strings"

	"golang.org/x/net/html"

	"github.com/juju/errors"
)

// Extractor extracts URLs.
type Extractor interface {
	Extract(ctx context.Context, link string) ([]*neturl.URL, error)
}

// HTTPClient is an abstraction for an HTTP client.
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// HTMLExtractor extracts URLs that are used to link one HTML page to others.
// In other words, it extracts hyperlinks from <a> HTML tags.
type HTMLExtractor struct {
	httpClient HTTPClient
}

// NewHTMLExtractor returns an instance of HTMLExtractor.
func NewHTMLExtractor(httpClient HTTPClient) *HTMLExtractor {
	return &HTMLExtractor{
		httpClient: httpClient,
	}
}

// Extract extracts all URLs that are used to link one HTML page to others, given a web link.
func (e *HTMLExtractor) Extract(ctx context.Context, link string) ([]*neturl.URL, error) {
	document, statusCode, err := e.requestHTML(ctx, link)
	if err != nil {
		return nil, errors.Trace(err)
	}

	parentURL, err := neturl.Parse(link)
	if err != nil {
		return nil, errors.Trace(err)
	}

	urls, err := e.extractURLs(parentURL.Host, document)
	if err != nil {
		return nil, errors.Trace(err)
	}

	if statusCode == http.StatusTooManyRequests {
		return urls, ErrTooManyRequests
	}

	return urls, nil
}

// requestHTML gets the HTML page from a link. It assumes that, even if the response got
// an error status code (i.e. client/server codes - 400=<code<599), since the error page
// might have html tags.
func (e *HTMLExtractor) requestHTML(ctx context.Context, link string) (*html.Node, int, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, link, http.NoBody)
	if err != nil {
		return nil, 0, errors.Trace(err)
	}

	resp, err := e.httpClient.Do(req)
	if err != nil {
		return nil, 0, errors.Trace(err)
	}

	defer resp.Body.Close()

	htmlNode, err := html.Parse(resp.Body)
	if err != nil {
		return nil, 0, errors.Trace(err)
	}

	return htmlNode, resp.StatusCode, nil
}

func (e *HTMLExtractor) extractURLs(domain string, node *html.Node) ([]*neturl.URL, error) {
	var out []*neturl.URL

	if node.Type == html.ElementNode && node.Data == "a" {
		for _, attr := range node.Attr {
			if attr.Key != "href" {
				continue
			}

			link := strings.ReplaceAll(attr.Val, " ", "")

			u, err := neturl.Parse(link)
			if err != nil {
				return nil, errors.Trace(err)
			}

			u = formatURL(domain, u)

			out = append(out, u)
		}
	}

	for child := node.FirstChild; child != nil; child = child.NextSibling {
		urls, err := e.extractURLs(domain, child)
		if err != nil {
			return nil, errors.Trace(err)
		}

		if len(urls) != 0 {
			out = append(out, urls...)
		}
	}

	return out, nil
}

func formatURL(parentLink string, url *neturl.URL) *neturl.URL {
	if url.Host == "" {
		url.Host = parentLink
	}

	if url.Scheme == "" {
		url.Scheme = "https"
	}

	return url
}
