package url

import (
	"bufio"
	"context"
	"io"
	"net/http"
	neturl "net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtractor_Extract(t *testing.T) {
	t.Run("too_many_requests", func(t *testing.T) {
		htmlFile, err := os.Open("../../static/test.html")
		if err != nil {
			t.Fatal(err)
		}
		defer tFileClose(t, htmlFile)

		httpClient := &mockHTTPClient{file: htmlFile}

		extractor := NewHTMLExtractor(httpClient)

		urls, err := extractor.Extract(context.Background(), "http://429.com")

		links := make([]string, 0, len(urls))
		for _, url := range urls {
			links = append(links, url.String())
		}

		assert.ErrorIs(t, ErrTooManyRequests, err)
		assert.Equal(t, []string{
			"https://test.me/",
			"https://robot.hetzner.com/server",
			"https://crawler-test.com/",
			"ftp://xyz",
			"https://429.com/index",
			"thisdoesntevenexists://whatever",
			"https://gotootherdomain.com/",
		}, links)
	})

	t.Run("html", func(t *testing.T) {
		htmlFile, err := os.Open("../../static/test.html")
		if err != nil {
			t.Fatal(err)
		}
		defer tFileClose(t, htmlFile)

		httpClient := &mockHTTPClient{file: htmlFile}

		extractor := NewHTMLExtractor(httpClient)

		urls, err := extractor.Extract(context.Background(), "http://example.com")

		links := make([]string, 0, len(urls))
		for _, url := range urls {
			links = append(links, url.String())
		}

		assert.Nil(t, err)
		assert.Equal(t, []string{
			"https://test.me/",
			"https://robot.hetzner.com/server",
			"https://crawler-test.com/",
			"ftp://xyz",
			"https://example.com/index",
			"thisdoesntevenexists://whatever",
			"https://gotootherdomain.com/",
		}, links)
	})

	t.Run("json", func(t *testing.T) {
		file, err := os.Open("../../static/test.json")
		if err != nil {
			t.Fatal(err)
		}
		defer tFileClose(t, file)

		httpClient := &mockHTTPClient{file: file}

		extractor := NewHTMLExtractor(httpClient)

		urls, err := extractor.Extract(context.Background(), "http://example.com")
		assert.Nil(t, err)
		assert.Empty(t, urls)
	})
}

func TestExtractor_formatURL(t *testing.T) {
	url := formatURL("example.com", &neturl.URL{
		Scheme: "",
		Host:   "",
		Path:   "random-path",
	})

	assert.Equal(t, "https://example.com/random-path", url.String())
}

func tFileClose(t *testing.T, file *os.File) {
	t.Helper()
	err := file.Close()
	if err != nil {
		t.Fatal(err)
	}
}

type mockHTTPClient struct {
	file *os.File
}

func (m *mockHTTPClient) Do(request *http.Request) (*http.Response, error) {
	if request.URL.String() == "http://429.com" {
		return &http.Response{
			StatusCode: http.StatusTooManyRequests,
			Body:       io.NopCloser(bufio.NewReader(m.file)),
		}, nil
	}

	if request.URL.String() != "http://example.com" {
		return nil, assert.AnError
	}

	resp := &http.Response{
		StatusCode: http.StatusOK,
		Body:       io.NopCloser(bufio.NewReader(m.file)),
	}

	return resp, nil
}
