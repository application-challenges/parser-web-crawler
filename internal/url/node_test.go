package url

import (
	neturl "net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewNode(t *testing.T) {
	urls := map[string][]*neturl.URL{
		"//depth2/second": {
			{Host: "depth3", Path: "first"},
		},
		"//depth3/first": {
			{Host: "depth4", Path: "first"},
		},
		"parent": {
			{Host: "depth2", Path: "first"},
			{Host: "depth2", Path: "second"},
		},
	}

	node := NewNode("parent", urls)

	assert.Equal(t, "parent", node.Link)
	assert.Len(t, node.Children, 2)

	parentFirstChild := node.Children[0]
	assert.Equal(t, "//depth2/first", parentFirstChild.Link)
	assert.Len(t, parentFirstChild.Children, 0)

	parentSecondChild := node.Children[1]
	assert.Equal(t, "//depth2/second", parentSecondChild.Link)
	assert.Len(t, parentSecondChild.Children, 1)

	secondChildFirstChild := parentSecondChild.Children[0]
	assert.Equal(t, "//depth3/first", secondChildFirstChild.Link)
	assert.Len(t, secondChildFirstChild.Children, 1)
}
