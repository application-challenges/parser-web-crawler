package url

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrinter_Print(t *testing.T) {
	node := &Node{
		Link: "parent",
		Children: []*Node{
			{
				Link: "depth2First",
			},
			{
				Link: "depth2Second",
				Children: []*Node{
					{
						Link: "depth3Single",
					},
				},
			},
			{
				Link: "depth2Third",
			},
		},
	}

	mockWriter := &mockWriter{log: make([]string, 0, 5)}
	printer := NewPrinter(mockWriter)

	err := printer.Print(node)
	assert.Nil(t, err)
	assert.Equal(t, []string{
		"-> parent\n",
		"  # depth2First\n",
		"  -> depth2Second\n",
		"    # depth3Single\n",
		"  # depth2Third\n",
	}, mockWriter.log)
}

type mockWriter struct {
	log []string
}

func (m *mockWriter) Write(p []byte) (n int, err error) {
	wrote := string(p)
	m.log = append(m.log, wrote)

	return 0, nil
}
