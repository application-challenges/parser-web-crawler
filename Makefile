.PHONY: test fmt vet

PROJECT_NAME := parser-web-crawler
FOLDER_BIN = $(CURDIR)/bin
GO_TEST = go test ./...
GO_FILES = $(shell go list ./... | grep -v /vendor/)

start:
	 go run cmd/crawler/main.go -link https://parserdigital.com/ -httpTimeout 15

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(CURDIR)/cmd/crawler/main.go

test:
	$(GO_TEST) -coverprofile cover.out

test-cover: test
	go tool cover -func cover.out

test-race:
	$(GO_TEST) -race

fmt:
	go fmt $(GO_FILES)

vet:
	go vet $(GO_FILES)

test-all: fmt vet test-race

mod:
	go mod tidy
